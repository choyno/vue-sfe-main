import Vue from 'vue'
import Vuex from 'vuex'
import Cookie from 'js-cookie'
import _ from 'lodash'

const defaultState = {}
const hasCookie = !_.isUndefined(Cookie.get(process.env.VUE_APP_COOKIE_NAME))

if (hasCookie) {
  Object.assign(defaultState, JSON.parse(Cookie.get(process.env.VUE_APP_COOKIE_NAME)))
} else {
  Object.assign(defaultState, { isAuth: false, token: '' })
}

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    ...defaultState
  },
  mutations: {
    login (state, payload) {
      const data = { isAuth: true, token: payload }
      Object.assign(state, { ...data })
      Cookie.set(process.env.VUE_APP_COOKIE_NAME, data)
    },
    logout (state) {
      const data = { isAuth: null, token: '' }
      Object.assign(state, { ...data })
      Cookie.remove(process.env.VUE_APP_COOKIE_NAME)
    },
    setAuth (state, payload) {
      Object.assign(state, { ...payload })
    }
  },
  actions: {
  },
  getters: {
    getIsAuth: state => state.isAuth,
    getToken: state => state.token
  }
})
