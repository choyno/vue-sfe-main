import SmoothScroll from 'smooth-scroll'

const smoothScroll = {}
smoothScroll.install = function (Vue, options) {
  Vue.prototype.$smoothScroll = function (options) {
    return new SmoothScroll(options)
  }
}

export default smoothScroll
