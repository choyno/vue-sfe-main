export default function (to, from, next, store) {
  if (to.name === 'Error') {
    next()
  } else if ((to.name === 'ViewPost' || to.name === 'EditPost') && !/^\d+$/.test(to.params.id)) {
    next({
      name: 'Error',
      params: { 0: to.path }
    })
  } else if (to.name === 'EditPost' && !store.getters.getIsAuth) {
    next({
      name: 'Error',
      params: { 0: to.path }
    })
  } else if (to.name === 'NewPost' && !store.getters.getIsAuth) {
    next({ name: 'Home' })
  } else {
    next()
  }
}
