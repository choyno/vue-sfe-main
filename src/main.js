import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import VueApollo from 'vue-apollo'
import Vue2Filters from 'vue2-filters'
import Vuelidate from 'vuelidate'
import VueMeta from 'vue-meta'
import lineClamp from 'vue-line-clamp'
import moment from 'moment'
import { ApolloClient } from 'apollo-client'
import { createHttpLink } from 'apollo-link-http'
import { InMemoryCache } from 'apollo-cache-inmemory'
import SmoothScroll from '@/plugin/smooth-scroll'
import './filters'
moment.suppressDeprecationWarnings = true

Vue.use(lineClamp)
Vue.use(Vuelidate)
Vue.use(Vue2Filters)
Vue.use(VueApollo)
Vue.use(VueMeta)
Vue.use(SmoothScroll)

const httpLink = createHttpLink({ uri: 'http://localhost:4000/graphql' })
const cache = new InMemoryCache()
const apolloClient = new ApolloClient({ link: httpLink, cache })
const apolloProvider = new VueApollo({ defaultClient: apolloClient })

Vue.config.productionTip = false

new Vue({
  apolloProvider,
  router,
  store,
  render: h => h(App)
}).$mount('#app')
