import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '../store'
import Home from '@/views/Home'
import Error from '@/views/Error'
import ViewPost from '@/views/ViewPost'
import EditPost from '@/views/EditPost'
import NewPost from '@/views/NewPost'
import authGuard from '../middleware/authGuard'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/view/:id',
    name: 'ViewPost',
    component: ViewPost
  },
  {
    path: '/edit/:id',
    name: 'EditPost',
    component: EditPost
  },
  {
    path: '/new',
    name: 'NewPost',
    component: NewPost
  },
  {
    path: '*',
    name: 'Error',
    component: Error
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {
  authGuard(to, from, next, store)
})

export default router
